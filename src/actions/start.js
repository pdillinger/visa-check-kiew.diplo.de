"use strict";

const Composer = require("telegraf/composer");
const composer = new Composer();
const Markup = require("telegraf/markup");

composer.start(async (ctx) => {
  return ctx.reply(
    "Welcome to the VISA checker bot",
    Markup.inlineKeyboard([
      Markup.callbackButton("Check for VISA number 1917033", "fetch"),
    ]).extra()
  );
});

module.exports = (bot) => {
  bot.use(composer.middleware());
};
