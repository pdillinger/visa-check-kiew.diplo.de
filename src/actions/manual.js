"use strict";

const Composer = require("telegraf/composer");
const composer = new Composer();
const Markup = require("telegraf/markup");
const fetchPDF = require("../components/download-pdf");

composer.action("fetch", async (ctx) => {
  const chatId = ctx.chat.id;
  const result = await fetchPDF();

  await ctx.answerCbQuery(result);
});

module.exports = (bot) => {
  bot.use(composer.middleware());
};
