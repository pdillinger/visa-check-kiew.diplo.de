"use strict";

const fs = require("fs").promises;
const pdf = require("pdf-parse");

const TXT_FILE_NAME = "visa_numbers.txt";
const REGEXP_VISA_DATA = /^Barcode\/(.*|[\n\r])+/gim;

const isDescription = (s) => /^([a-zA-Z]|\d{2}.\d{2}.\d{2})(.+)/gim.test(s);

const isNumber = (n) =>
  /^\d{7}$/gim.test(n) ||
  /^\d{7}\s?\(neu\s?\d{7}\)\,{1}\s{1}\d{7}/gim.test(n) ||
  /^\(neu\s?\d{7}\)/gim.test(n) ||
  /^\d{7}\s?\(neu\s?\d{7}\)\,? ?\d{7}$/gim.test(n);

const isNumberDesc = (s) =>
  /^\d{7}\ {1}[A-Z]{2}.*(\w|\))$/gim.test(s) ||
  /^\d{7}\,\d{7}.*(\w|\))$/gim.test(s) ||
  /^\d{7}\ \(neu\s?\d{7}\)\ [A-Z]{2}.*(\w|\))$/gim.test(s);

const filterWords = [
  "ANSCHRIFT TEL : E-MAIL:",
  "wul. Lwa Tolstoho, 57, Businesszentrum „101 Tower“, 22. Etage + 380 44 2811400 visa@kiew.diplo.de",
  "01901 Kiew FAX:  INTERNET:",
  "ANSCHRIFT + 49 30 5000 67099 www.kiew.diplo.de",
  "Barcode/",
  "Код",
  "mitzubringende Dokumente/",
  "Документи, які необхідно мати при",
  "собі",
  "Visum SPÄTESTENS",
  "abholen bis/",
  "Забрати візу ДО",
  " ",
];

function convertStringToArray(text) {
  return text.split("\n").map((x) => x.trim());
}

function removeRedundantWords(stopWords) {
  return (data) => data.filter((s) => !stopWords.includes(s) && s !== "");
}

function getTextWithVisas(regexp) {
  return (text) => {
    const result = text.match(regexp);
    return result ? result[0] : "";
  };
}

function addSeparation(x) {
  if (isNumber(x)) return "|";
  if (isDescription(x)) return "=";

  return "@";
}

function isNewVisaNumber(curr, index, array) {
  if (index - 1 === -1) return false;
  return (
    (isDescription(array[index - 1]) || isNumberDesc(array[index - 1])) &&
    isNumber(curr)
  );
}

function prettifyData(data) {
  const result = data
    .reduce((acc, curr, index, array) => {
      const separation = index !== 0 ? addSeparation(curr) : "";
      const isEdge = isNewVisaNumber(curr, index, array);

      return `${acc}${isEdge ? "@" : separation}${curr}`;
    }, "")
    .split("@");
  return result;
}

function getTextData(x) {
  return x.text;
}

function error(type) {
  return (err) => {
    if (err) {
      console.log(`ERROR in ${type} -> `, err);
    }
  };
}

function writeToFile(data) {
  fs.writeFile(
    `public/${TXT_FILE_NAME}`,
    data.join("\n"),
    error("writeToFile")
  );
  return data;
}

function searchForId(id) {
  return (data) => data.find((x) => x.includes(id));
}

function message(id) {
  return (str) => (str ? str : `Can't find a VISA with given number ${id}`);
}

function parsePDF() {
  return fs.readFile("public/visa.pdf").then((data) => {
    return (
      pdf(data)
        .then(getTextData)
        .then(getTextWithVisas(REGEXP_VISA_DATA))
        .then(convertStringToArray)
        .then(removeRedundantWords(filterWords))
        .then(prettifyData)
        // .then(writeToFile)
        .then(searchForId("1917033"))
        .then(message("1917033"))
        .catch(error("parsePDF"))
    );
  });
}

module.exports = {
  parsePDF,
  isNewVisaNumber,
  convertStringToArray,
  removeRedundantWords,
  getTextWithVisas,
  prettifyData,
  addSeparation,
  isDescription,
  isNumber,
  isNumberDesc,
};
