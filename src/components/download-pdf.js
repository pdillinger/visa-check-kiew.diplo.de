"use strict";
const axios = require("axios");
const fs = require("fs").promises;
const CronJob = require("cron").CronJob;
const puppeteer = require("puppeteer");
const Composer = require("telegraf/composer");
const Markup = require("telegraf/markup");
const { parsePDF } = require("./process-pdf");

const PATH_TO_VISA_PDF = "public/visa.pdf";
const PAGE_URL =
  "https://kiew.diplo.de/ua-uk/service/05-VisaEinreise/visumabholung/1283752";

async function fetch() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(PAGE_URL);

  // execute standard javascript in the context of the page.
  const pdfURL = await page.$$eval("a.rte__anchor.i-pdf", (el) => el[0].href);

  await browser.close();

  return axios
    .get(pdfURL, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/pdf",
      },
    })
    .then((x) => x.data)
    .then((data) => {
      return fs.writeFile(PATH_TO_VISA_PDF, data);
    })
    .then(parsePDF)
    .catch((error) => {
      console.log(error);
      return error;
    });
}

module.exports = fetch;
