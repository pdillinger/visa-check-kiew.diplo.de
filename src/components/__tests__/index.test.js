const service = require("./index.js");

const inputData = [
  "1911271",
  "1911272",
  "RP, ES",
  "RP,ES, Geburtsurkunde mit Apostille",
  "und beglaubigter Übersetzung + 1",
  "Kopie",
  "03.04.2020",
  "1911332 RP, ES 11.02.2020",
  "1911338 RP, ES, KV wenn Einreise nach 09.04.2020 09.04.2020",
  "1911390 RP, ES 03.06.2020",
  "1911396 RP, ES, KV 29.04.2020",
  "1911587 RP, ES 05.06.2020",
  "1911592 RP, ES, Gerichtsbeschluss, dass dem",
  "Vater Sorgerecht entzogen wurde und",
  "Gerichtsbeschluss, dass Kind für",
  "ständigen Wohnsitz nach Deutschland",
  "ausreisen darf, KV, wenn Einreise nach",
  "30.11.2019",
  "01.07.2020",
  "1911599 RP, ES 05.06.2020",
  "1911635 RP, ES, KV 05.05.2020",
  "1911271",
  "1911272",
  "RP, ES",
  "RP,ES, Geburtsurkunde mit Apostille",
  "und beglaubigter Übersetzung + 1",
  "Kopie",
  "03.04.2020",
  "1911957 RP, ES 05.06.2020",
  "1911983 RP, ES, Berufserlaubnis, aktueller AV, 24.04.2020",
  "1915488 RP, ES, PV 05.06.2020",
  "1915499, 1915519 RP, ES, KV,PV des Kindes , TER 20.03.2020",
  "1915535",
  "1915539",
  "RP, ES, KV",
  "RP, ES, KV, PV",
  "05.06.2020",
];

test("split text by a new line", () => {
  const result = service.convertStringToArray(`one
  two
  three`);

  expect(result).toMatchInlineSnapshot(`
    Array [
      "one",
      "two",
      "three",
    ]
  `);
});

test("builds array with the human readable visa status", () => {
  const result = [
    "1911271|1911272=RP, ES=RP,ES, Geburtsurkunde mit Apostille=und beglaubigter Übersetzung + 1=Kopie=03.04.2020",
    "1911332 RP, ES 11.02.2020",
    "1911338 RP, ES, KV wenn Einreise nach 09.04.2020 09.04.2020",
    "1911390 RP, ES 03.06.2020",
    "1911396 RP, ES, KV 29.04.2020",
    "1911587 RP, ES 05.06.2020",
    "1911592 RP, ES, Gerichtsbeschluss, dass dem=Vater Sorgerecht entzogen wurde und=Gerichtsbeschluss, dass Kind für=ständigen Wohnsitz nach Deutschland=ausreisen darf, KV, wenn Einreise nach=30.11.2019=01.07.2020",
    "1911599 RP, ES 05.06.2020",
    "1911635 RP, ES, KV 05.05.2020",
    "1911271|1911272=RP, ES=RP,ES, Geburtsurkunde mit Apostille=und beglaubigter Übersetzung + 1=Kopie=03.04.2020",
    "1911957 RP, ES 05.06.2020",
    "1911983 RP, ES, Berufserlaubnis, aktueller AV, 24.04.2020",
    "1915488 RP, ES, PV 05.06.2020",
    "1915499, 1915519 RP, ES, KV,PV des Kindes , TER 20.03.2020",
    "1915535|1915539=RP, ES, KV=RP, ES, KV, PV=05.06.2020",
  ];

  expect(service.combine).toBeDefined();
  expect(service.combine(inputData)).toEqual(result);
});

test("gets specifict separator based on parsed string", () => {
  expect(service.addSeparation("1911271")).toEqual("|");
  expect(service.addSeparation("Vater Sorgerecht entzogen wurde und")).toEqual(
    "="
  );
  expect(service.addSeparation("30.11.2019")).toEqual("=");
  expect(service.addSeparation("1911599 RP, ES 05.06.2020")).toEqual("@");
});

test("regexp checks", () => {
  expect(service.isDescription(inputData[4])).toBe(true);
  expect(service.isDescription(inputData[7])).toBe(false);
  expect(service.isNumber(inputData[0])).toBe(true);
  expect(service.isNumber(inputData[4])).toBe(false);
  expect(service.isNumberDesc(inputData[7])).toBe(true);
  expect(service.isNumberDesc(inputData[1])).toBe(false);
});

test("puts a new row symbol for the new visa number", () => {
  expect(service.isNewVisaNumber("", 0, [])).toEqual(false);
  expect(
    service.isNewVisaNumber("1911957", 0, ["1911587 RP, ES 05.06.2020"])
  ).toEqual(false);
});
