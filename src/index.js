"use strict";

const Telegraf = require("telegraf");
const startAction = require("./actions/start");
const manualAction = require("./actions/manual.js");

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);

bot.use(Telegraf.log());

startAction(bot);
manualAction(bot);

bot.catch((err, ctx) => {
  console.log(`Ooops, encountered an error for ${ctx.updateType}`, err);
});

bot.launch();
