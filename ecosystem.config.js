module.exports = {
  apps: [
    {
      name: "VISABOT",
      script: "src/index.js",

      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      args: "",
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "development",
        TELEGRAM_BOT_TOKEN: "1073817484:AAHdH8vekqG2hDY-8kPWADY2CCv1j_w9P1I",
      },
      env_production: {
        NODE_ENV: "production",
        TELEGRAM_BOT_TOKEN: "1073817484:AAHdH8vekqG2hDY-8kPWADY2CCv1j_w9P1I",
      },
    },
  ],

  deploy: {
    production: {
      user: "node",
      host: "212.83.163.1",
      ref: "origin/master",
      repo: "git@github.com:repo.git",
      path: "/var/www/production",
      "post-deploy":
        "npm install && pm2 reload ecosystem.config.js --env production",
    },
  },
};
